/* eslint-disable jsx-a11y/no-href*/

import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Grid, Alert, Button } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Roles } from 'meteor/alanning:roles';
import { Bert } from 'meteor/themeteorchef:bert';
import Navigation from '../../components/Navigation/Navigation';
import Index from '../../pages/Index/Index';
import StepOne from '../../pages/StepOne/StepOne';
import StepOneTwo from '../../pages/StepOne/StepOneTwo';
import StepTwo from '../../pages/StepTwo/StepTwo';
import StepTwoOne from '../../pages/StepTwo/StepTwoOne';
import StepTwoTwo from '../../pages/StepTwo/StepTwoTwo';
import StepThree from '../../pages/StepThree/StepThree';
import Success from '../../pages/StepThree/Success';
import Fail from '../../pages/StepThree/Fail';
import Pricing from '../../pages/Pricing/Pricing';
import Resources from '../../pages/Resources/Resources';
import Fingerprint from '../../pages/Resources/Fingerprint';
import Authenticite from '../../pages/Resources/Authenticite';
import Ancrage from '../../pages/Resources/Ancrage';
import Contact from '../../pages/Contact/Contact';
import NotFound from '../../pages/NotFound/NotFound';
import Footer from '../../components/Footer/Footer';
import Terms from '../../pages/Terms/Terms';
import Privacy from '../../pages/Privacy/Privacy';

import './App.scss';

const handleResendVerificationEmail = (emailAddress) => {
  Meteor.call('users.sendVerificationEmail', (error) => {
    if (error) {
      Bert.alert(error.reason, 'danger');
    } else {
      Bert.alert(`Check ${emailAddress} for a verification link!`, 'success');
    }
  });
};

const App = props => (
  <Router>
    {!props.loading ? <div className="App">
      {props.userId && !props.emailVerified ? <Alert className="verify-email text-center"><p>Hey friend! Can you <strong>verify your email address</strong> ({props.emailAddress}) for us? <Button bsStyle="link" onClick={() => handleResendVerificationEmail(props.emailAddress)} href="#">Re-send verification email</Button></p></Alert> : ''}
      <Navigation {...props} />
      <Grid>
        <Switch>
          <Route exact path="/" render={() => (
              <Redirect to="/index"/>
          )}/>
          <Route exact name="index" path="/index" component={Index} />
          <Route exact name="stepone" path="/stepone" component={StepOne} />
          <Route exact name="steponetwo" path="/steponetwo" component={StepOneTwo} />
          <Route exact name="steptwo" path="/steptwo" component={StepTwo} />
          <Route exact name="steptwoone" path="/steptwoone" component={StepTwoOne} />
          <Route exact name="steptwotwo" path="/steptwotwo" component={StepTwoTwo} />
          <Route exact name="stepthree" path="/stepthree" component={StepThree} />
          <Route exact name="success" path="/success" component={Success} />
          <Route exact name="fail" path="/fail" component={Fail} />
          <Route exact name="pricing" path="/pricing" component={Pricing} />
          <Route exact name="resources" path="/resources" component={Resources} />
          <Route exact name="fingerprint" path="/resources/fingerprint" component={Fingerprint} />
          <Route exact name="ancrage" path="/resources/ancrage" component={Ancrage} />
          <Route exact name="authenticite" path="/resources/authenticite" component={Authenticite} />
          <Route exact name="contact" path="/contact" component={Contact} />
          <Route name="terms" path="/terms" component={Terms} />
          <Route name="privacy" path="/privacy" component={Privacy} />
          <Route component={NotFound} />
        </Switch>
      </Grid>
      <Footer />
    </div> : ''}
  </Router>
);

App.defaultProps = {
  userId: '',
  emailAddress: '',
};

App.propTypes = {
  loading: PropTypes.bool.isRequired,
  userId: PropTypes.string,
  emailAddress: PropTypes.string,
  emailVerified: PropTypes.bool.isRequired,
};

const getUserName = name => ({
  string: name,
  object: `${name.first} ${name.last}`,
}[typeof name]);

export default createContainer(() => {
  const loggingIn = Meteor.loggingIn();
  const user = Meteor.user();
  const userId = Meteor.userId();
  const loading = !Roles.subscription.ready();
  const name = user && user.profile && user.profile.name && getUserName(user.profile.name);
  const emailAddress = user && user.emails && user.emails[0].address;

  return {
    loading,
    loggingIn,
    authenticated: !loggingIn && !!userId,
    name: name || emailAddress,
    roles: !loading && Roles.getRolesForUser(userId),
    userId,
    emailAddress,
    emailVerified: user && user.emails ? user && user.emails && user.emails[0].verified : true,
  };
}, App);
