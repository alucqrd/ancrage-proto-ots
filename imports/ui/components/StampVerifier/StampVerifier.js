/* eslint-disable max-len, no-return-assign */

import React from 'react';
import {Button, Col, Row} from 'react-bootstrap';
import CryptoJS from 'crypto-js';
import VerificationResult from '../../components/VerificationResult/VerificationResult';

import './StampVerifier.scss';

class StampVerifier extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      otsFile: undefined,
      fileHash: undefined,
      verifyResult: "none",
      loadingResult: false,
    };

    $(function() {
      $("#doc").change(function (){
        var fileName = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '')
        $(".uploaded-doc-text").html("&#10003; " + fileName + " ajouté avec succès");
      });
   });

   $(function() {
      $("#receipt").change(function (){
        $(".uploaded-doc-receipt").html("&#10003; Reçu ajouté avec succès");
      });
   });

    this.handleSubmitOtsFile = this.handleSubmitOtsFile.bind(this);
    this.handleSubmitFileChecked = this.handleSubmitFileChecked.bind(this);
    this.submitVerification = this.submitVerification.bind(this);
  }

  handleSubmitOtsFile(event) {
    let files = event.target.files;
    let that = this;
    let otsFileArrayBuffer;
    _.each(files, function (file) {
      // Handle OTS file
      if (file.name.split('.').pop().toLowerCase() == 'ots') {
        let reader = new FileReader();

        reader.onload = function (fileLoadEvent) {
          let arrayBuffer = this.result;
          otsFileArrayBuffer = new Uint8Array(arrayBuffer);
          that.setState({
            otsFile: otsFileArrayBuffer,
          });
        };

        // Read in the image file as a data URL.
        reader.readAsArrayBuffer(file);
      }
    });
  }

  handleSubmitFileChecked(event) {
    let files = event.target.files;
    let that = this;
    let fileHashChecked;
    _.each(files, function(file) {
      let reader = new FileReader();
      // Closure to capture the file information.
      reader.onloadend = (function(theFile) {
        return function (e) {
          let arrayBuffer = e.target.result;
          let i8a = new Uint8Array(arrayBuffer);
          let a = [];
          for (let i = 0; i < i8a.length; i += 4) {
            a.push(i8a[i] << 24 | i8a[i + 1] << 16 | i8a[i + 2] << 8 | i8a[i + 3]);
          }

          let wordArray = CryptoJS.lib.WordArray.create(a, i8a.length);
          fileHashChecked = CryptoJS.SHA256(wordArray);
          that.setState({
            fileHash: fileHashChecked.toString(CryptoJS.enc.Hex),
          });
        }
      })(file);
      reader.readAsArrayBuffer(file);
    });
  }

  submitVerification() {
    console.log(this.state.fileHash);
    console.log(this.state.otsFile);
    Meteor.call('opentimestamps.verify', this.state.fileHash, this.state.otsFile, (error, result) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
        this.setState({loadingResult: false});
      }
      else {
        this.setState({verifyResult: result});
        this.setState({loadingResult: false});
      }
    });
    this.setState({loadingResult: true});

  }

  render() {
    return(
      <div className="StampVerifier">
      <Row>
        <Col md={5} mdOffset={1} className="dropzone-background dropzone-left">
          <p className="dropzone-title">Document à vérifier</p>
          <p className="uploaded-doc-text">Aucun document sélectionné</p>
        <div id="dropzone">
          <div><i className="fa fa-download fa-3x" aria-hidden="true" /></div>
          <input type="file" id="doc" onChange={this.handleSubmitFileChecked}/>
        </div>
        </Col>
        <Col md={5} className="dropzone-background dropzone-right">
          <p className="dropzone-title">Reçu (au format .OTS)</p>
          <p className="uploaded-doc-receipt">Aucun reçu sélectionné</p>
        <div id="dropzone">
          <div><i className="fa fa-download fa-3x" aria-hidden="true" /></div>
          <input type="file" id="receipt" accept=".ots" onChange={this.handleSubmitOtsFile}/>
        </div>
        </Col>
        </Row><br />
        <Row>
          <Col>
        <Button
          bsStyle="success"
          className="attente-confirmation btn-lg centered"
          onClick={this.submitVerification}>
          Verifier le document
        </Button>
        </Col>
        </Row>
        { !this.state.loadingResult ?
          <VerificationResult verifyResult={this.state.verifyResult}/> :
          <div className="sk-cube-grid">
          <div className="sk-cube sk-cube1"></div>
          <div className="sk-cube sk-cube2"></div>
          <div className="sk-cube sk-cube3"></div>
          <div className="sk-cube sk-cube4"></div>
          <div className="sk-cube sk-cube5"></div>
          <div className="sk-cube sk-cube6"></div>
          <div className="sk-cube sk-cube7"></div>
          <div className="sk-cube sk-cube8"></div>
          <div className="sk-cube sk-cube9"></div>
        </div>
        }
      </div>
    );
  }
}

export default StampVerifier;
