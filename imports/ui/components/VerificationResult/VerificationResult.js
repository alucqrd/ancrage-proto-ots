import {Meteor} from 'meteor/meteor';
import {Email} from 'meteor/email';
import React from 'react';
import {Button, Row, Col} from 'react-bootstrap';

class VerificationResult extends React.Component {

  selectRender() {
    if(this.props.verifyResult == "none")
      return (
        null
      );
    
    else if(this.props.verifyResult == "Pas d'ancrage trouvé dans la blockchain.")
      return (
        <Row>
          <Col xs={12} md={12} className="receipt-request-frame">
            <div className="text-center">
              <i className="fa fa-times-circle-o fa-5x" aria-hidden="true"/><br/><br/>
              <p>Aucune correspondance n'a été trouvée.
                <br/>Assurez-vous d'avoir utilisé le document source et le reçu lié à ce document.</p>
            </div>
          </Col>
        </Row>
      );

      else if(this.props.verifyResult == "Le fichier .ots de reçu ne correspond pas au fichier donné en entrée.")
      return(
        <Row>
        <Col xs={12} md={12} className="receipt-request-frame">
          <div className="text-center">
          <i className="fa fa-times-circle-o fa-5x" aria-hidden="true"></i><br /><br />
          <p>Aucune correspondance n'a été trouvée. <br />Assurez-vous d'avoir utilisé le document source et le reçu lié à ce document.</p>
          </div>
        </Col>
      </Row>
      );

      else
        return(
          <Row>
            <Col xs={12} md={12} className="receipt-request-frame">
              <div className="text-center">
                <i className="fa fa-check-circle-o fa-5x" aria-hidden="true"/>
                <h2>Ancrage vérifié avec succès !</h2>
                <p>{this.props.verifyResult}</p>
              </div>
            </Col>
          </Row>
        );
  }

  render() {
    return(
      <div className="VerificationResult">
        {this.selectRender()}
      </div>
    );

    /*return (
      <div className="VerificationResult">
         < br />
      <Row>
        <Col md={4} mdOffset={3}>
          <a href="/steptwo">
            <button className="btn btn-success centered">Ensavoir plus sur la vérification blockchain</button>
          </a>
        </Col>
        <Col md={2}>
          <a href="/steptwoone">
            <button className="btn btn-success centered">Utiliser le service</button>
          </a>
        </Col>
      </Row>
      <br/>
      <Row>
        <Col>
          <a href="/steptree">
            <button className="btn btn-success centered">Recommencer la vérification d'un document</button>
          </a>
        </Col>
      </Row>
    </div>);*/
  }
}

export default VerificationResult;