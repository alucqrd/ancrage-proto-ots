import React from 'react';
import {Button, Row, Col} from 'react-bootstrap';
// import {SectionsContainer, Section, Header, Footer} from 'react-fullpage';
import { Link } from 'react-router-dom';
import '../../stylesheets/animate.css';
import './IndexContent.scss';

if (screen.width <= 767) {
    document.location = "http://blockchainpartner.fr";
    }

class IndexContent extends React.Component {
    render() {
        return (
            <div className="IndexContent">
                <Row>
                    <Col md={12}>
                        <section className="home-container">
                            <img className="logo" src="logo.png"/>
                            <h1 className="homepage-title">Certifiez vos données numériques</h1>
                            <div className="down-arrow centered">
                                <a data-scroll href="#why">
                                    <i className="fa fa-chevron-down fa-4x animated infinite bounce"></i>
                                </a>
                            </div>
                        </section>
                    </Col>
                </Row>
                <section id="why">
                    <h2>L’ancrage Blockchain, késaco ?</h2>
                    <h3>L'ancrage permet de <b>certifier ces données</b> et de garantir leur intégrité</h3><br />
                    <Row>
                        <Col className="homepage-paragraph" xs={12} md={12}>
                            <img src="ancrage.jpg" className="ancrage"/>
                            <p>
                                <b>Qu’est-ce que l’ancrage blockchain ?</b>
                            </p>

                            <p>L’ancrage blockchain utilise des procédés cryptographiques pour créer
                                l’empreinte numérique d’une donnée (document PDF, image JPEG, etc.) et
                                l’inscrire dans un registre blockchain. L’empreinte numérique est l’équivalent
                                de l’empreinte digitale, mais pour les données. A une donnée correspond une
                                empreinte numérique : si la donnée est modifiée ne serait-ce que d’un seul
                                caractère, son empreinte numérique sera totalement différente.</p><br />

                            <p>
                                <b>Pourquoi ancrer des données ?</b>
                            </p>

                            <p>Aujourd’hui une large partie des données (factures, contrats et avenants,
                                processus industriels, etc.) créées par les entreprises sont des données
                                numériques. L’ancrage blockchain permet de garantir l’intégrité de ces données
                                numériques. En d’autres termes, <b>l’ancrage blockchain permet de prouver qu’une donnée n’a pas été altérée, que
                                    ce soit de manière intentionnelle ou non</b>.</p>

                            <p>
                                <b>Pourquoi utiliser la Blockchain ?</b>
                            </p>

                            <p>La blockchain est par nature un registre indépendant de toute autorité
                                centrale, ce qui lui confère des propriétés de résistance à la corruption
                                inégalées, personne ne pouvant altérer une information une fois celle-ci
                                insérée.</p>

                            <ul>
                                <li>D’un point de vue légal, l’ancrage blockchain a aujourd’hui la même valeur
                                    juridique qu’un mail : il permet d’apporter au législateur un faisceau de
                                    preuves permettant de démontrer qu’une donnée n’a pas été corrompue.</li>

                                <li>D’un point de vue business, l’ancrage blockchain permet de garantir la
                                    conformité d’une donnée et ainsi lutter contre la falsification de celle-ci.</li>
                            </ul>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={5} mdOffset={5}>
                            <Link to={"/stepone"}>
                                <button className="btn btn-lg btn-success test-ancrage">Tester l'ancrage</button>
                            </Link>
                        </Col>
                    </Row>

                </section>
            </div>
        );
    }
}

export default IndexContent;
