import React from 'react';
import {LinkContainer} from 'react-router-bootstrap';
import {Nav, Navbar, NavDropdown, MenuItem, NavItem} from 'react-bootstrap';
import './PublicNavigation.scss';

const PublicNavigation = () => (
  <Navbar className="navbar-fixed-top">
    <Navbar.Brand>
      <LinkContainer to="/index">
      <img src="logo.png" href="/index" className="header-logo"/>
      </LinkContainer>
    </Navbar.Brand>
    <Nav pullRight>
      <LinkContainer to="/index">
        <NavItem eventKey={1} href="/index">Accueil</NavItem>
      </LinkContainer>
        <NavDropdown eventKey={2} title="Demo" id="dropdownmenu">
      <LinkContainer to="/stepone">
        <MenuItem eventKey={2.1}>Étape 1 : Certifier un document numérique</MenuItem>
        </LinkContainer>
        <LinkContainer to="/stepthree">
        <MenuItem eventKey={2.2}>Étape 2 : Vérifier un document</MenuItem>
        </LinkContainer>
        </NavDropdown>
      <LinkContainer to="/pricing">
        <NavItem eventKey={3} href="/pricing">Nos offres</NavItem>
      </LinkContainer>
      <LinkContainer to="/resources">
        <NavItem eventKey={4} href="/resources">Ressources</NavItem>
      </LinkContainer>
      <LinkContainer to="/contact">
        <NavItem eventKey={5} href="/contact">Contact</NavItem>
      </LinkContainer>
    </Nav>
  </Navbar>
);

export default PublicNavigation;
