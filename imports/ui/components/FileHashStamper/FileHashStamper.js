/* eslint-disable max-len, no-return-assign */

import React from 'react';
import CryptoJS from 'crypto-js';
import { Modal, Button } from 'react-bootstrap';
import './FileHashStamper.scss';

class FileHashStamper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayResultModal: false, // 0 = no display, 1 = cancelation, 2 = lock/unlock buttons
    };
  }

  handleSubmit(event) {
    let that = this;
    let files = event.target.files;
    _.each(files, function(file) {
      let reader = new FileReader();
      // Closure to capture the file information.
      reader.onloadend = (function(theFile) {
        return function(e) {
          let arrayBuffer = e.target.result;
          let i8a = new Uint8Array(arrayBuffer);
          let a = [];
          for (let i = 0; i < i8a.length; i += 4) {
            a.push(i8a[i] << 24 | i8a[i + 1] << 16 | i8a[i + 2] << 8 | i8a[i + 3]);
          }

          let wordArray = CryptoJS.lib.WordArray.create(a, i8a.length);
          let hash = CryptoJS.SHA256(wordArray);
          Meteor.call('opentimestamps.stampHash', hash.toString(CryptoJS.enc.Hex), (error, result) => {
            if (error) {
              Bert.alert(error.reason, 'danger');
            }
            else {
              let saveByteArray = (function () {
                let a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                return function (data, name) {
                  let blob = new Blob([data.buffer], {type: "application/octet-stream"}),
                    url = window.URL.createObjectURL(blob);
                  a.href = url;
                  a.download = name;
                  a.click();
                  window.URL.revokeObjectURL(url);
                };
              }());
              
              saveByteArray(result, "reçu_datatrust_"+file.name+".ots");
              that.setState({displayResultModal: true});
              //Bert.alert("Votre fichier a bien été ancré dans la blockchain et un reçu (fichier .ots) a été téléchargé par votre navigateur.", 'success');
            }
          });
        };

      })(file);
      reader.onerror = function(e) {
        console.error(e);
      };

      // Read in the image file as a data URL.
      reader.readAsArrayBuffer(file);
    });
  }

  render() {
    return(
    <div className="FileStamper">
      <div id="dropzone">
        <div><i className="fa fa-download fa-3x" aria-hidden="true" /></div>
        <input type="file" accept="image/png, application/pdf" onChange={this.handleSubmit.bind(this)}/>
      </div>
      <p className="dropzone-text">Cliquez ici pour sélectionner un fichier à vérifier ou glissez-le dans cette section</p>
      <Modal
        show={this.state.displayResultModal}
        onHide={() => this.setState({ displayResultModal: false })}
        container={this}
        aria-labelledby="contained-modal-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title">Ancrage confirmé</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p className="text-center">
            <i className="fa fa-check-square-o fa-4x" aria-hidden="true"/><br/>
            Votre fichier a bien été ancré dans la blockchain et un reçu (fichier .ots) a été enregistré dans le dossier "Téléchargements" de votre ordinateur.
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => this.setState({ displayResultModal: false })}>Ancrer un autre document</Button>
          <Button onClick={() => this.props.history.push('/stepthree')}>Vérifier le document</Button>
        </Modal.Footer>
      </Modal>
    </div>
    );
  }
}

export default FileHashStamper;
