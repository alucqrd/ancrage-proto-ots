import { Meteor } from 'meteor/meteor';
import { Email } from 'meteor/email';
import React from 'react';
import {
  Button,
  Row,
  Col,
  Checkbox,
  Radio,
  FormGroup,
  ControlLabel,
  FormControl,
  Form
} from 'react-bootstrap';

class ContactForm extends React.Component {

  handleSubmit() {
    Meteor.call("utility.submitContactForm", this.firstName.value, this.lastName.value, this.email.value, this.phone.value, this.company.value, (error, result) =>
    {
      if(!error)
        Bert.alert("Votre demande de contact a bien été soumise.", 'success');
      else
        Bert.alert(error.reason, 'danger');

    });
  }

  render() {
    return (
      <Form type="submit">
        <Row>
          <Col className="formField" md={4} mdOffset={4}>
            <FormGroup controlId="firstName">
              <ControlLabel>Prénom</ControlLabel>
              {' '}
              <FormControl inputRef={input => this.firstName = input} type="text"/>
            </FormGroup>
          </Col>
        </Row>
        {' '}
        <Row>
          <Col className="formField" md={4} mdOffset={4}>
            <FormGroup controlId="lastName">
              <ControlLabel>Nom</ControlLabel>
              {' '}
              <FormControl inputRef={input => this.lastName = input} type="text"/>
            </FormGroup>
          </Col>
        </Row>
        {' '}
        <Row>
          <Col className="formField" md={4} mdOffset={4}>
            <FormGroup controlId="email">
              <ControlLabel>Adresse email</ControlLabel>
              {' '}
              <FormControl inputRef={input => this.email = input} type="email"/>
            </FormGroup>
          </Col>
        </Row>
        {' '}
        <Row>
          <Col className="formField" md={4} mdOffset={4}>
            <FormGroup controlId="phone">
              <ControlLabel>Numéro de téléphone</ControlLabel>
              {' '}
              <FormControl inputRef={input => this.phone = input} type="tel"/>
            </FormGroup>
          </Col>
        </Row>
        {' '}
        <Row>
          <Col className="formField" md={4} mdOffset={4}>
            <FormGroup controlId="company">
              <ControlLabel>Entreprise</ControlLabel>
              <FormControl inputRef={input => this.company = input} type="text"/>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col mdOffset={4}>
            <Button className="btn btn-success" onClick={this.handleSubmit.bind(this)}>
              Envoyer
            </Button>
          </Col>
        </Row>
      </Form>
    );
  };
};

export default ContactForm;