import React from 'react';
import {Button, Col, Row} from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Success = () => (
  <div className="Success">
  <h1>Étape 2 : Vérifier un document</h1>

  <Row>
    <Col xs={12} md={12} className="receipt-request-frame">
      <div className="text-center">
      <i className="fa fa-check-circle-o fa-5x" aria-hidden="true"></i>
      <h2>Ancrage vérifié avec succès !</h2>
      <p>Le document a bien été ancré le XX/XX/XXXX à HH:MM.</p>
      </div>
    </Col>
  </Row><br/>

  <Row>
    <Col md={4} mdOffset={3}>
      <Link to={"/steptwo"}><button className="btn btn-success centered">Ensavoir plus sur la vérification blockchain</button></Link>
    </Col>
    <Col md={2}>
      <Link to={"/steptwoone"}><button className="btn btn-success centered">Utiliser le service</button></Link>
    </Col>
  </Row>

</div>/* End Success */
);

export default Success;