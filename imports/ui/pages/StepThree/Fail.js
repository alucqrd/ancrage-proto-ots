import React from 'react';
import { Col, Row} from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Fail = () => (
  <div className="Fail">
  <h1>Étape 2 : Vérifier un document</h1>

  <Row>
    <Col xs={12} md={12} className="receipt-request-frame">
      <div className="text-center">
      <i className="fa fa-times-circle-o fa-5x" aria-hidden="true"></i><br /><br />
      <p>Aucune correspondance n'a été trouvée. <br />Assurez-vous d'avoir utilisé le document source et le reçu lié à ce document.</p>
      </div>
    </Col>
  </Row><br/>

  <Row>
    <Col>
      <Link to={"/steptree"}><button className="btn btn-success centered">Recommencer la vérification d'un document</button></Link>
    </Col>
  </Row>

</div>/* End Fail */
);

export default Fail;