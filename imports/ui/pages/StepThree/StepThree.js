import React from 'react';
import {Button, Col, Row, Alert} from 'react-bootstrap';
import StampVerifier from '../../components/StampVerifier/StampVerifier';
import './StepThree.scss';

const StepOne = () => (
  <div className="StepOne">
  <h1>Étape 2 : vérifier un document</h1>
  <Row>
    <Col md={10} mdOffset={1}>
  <Alert bsStyle="warning" className="centered">
    <p className="text-center">Cliquez dans chacune des zones pour choisir vos fichiers ou bien déposez-les directement depuis votre dossier.</p>
  </Alert>
  </Col>
  </Row>
  <StampVerifier/>
</div>/* End StepOne */
);

export default StepOne;