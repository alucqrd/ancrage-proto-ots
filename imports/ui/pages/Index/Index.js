import React from 'react';
import IndexContent from '../../components/IndexContent/IndexContent.js'

const Index = () => (
  <div id="index">
    <IndexContent />
  </div>
);

export default Index;
