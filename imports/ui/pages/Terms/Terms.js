import React from 'react';
import Page from '../Page/Page';

const Terms = () => (
  <div className="Terms">
    <Page
      title="Mentions légales"
      subtitle="Dernière mise à jour le 28/11/2017"
      page="terms"
    />
  </div>
);

export default Terms;
