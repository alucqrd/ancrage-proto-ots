import React from 'react';
import {Button, Col, Row, Alert} from 'react-bootstrap';
import FileHashStamper from '../../components/FileHashStamper/FileHashStamper';
import './StepOne.scss';

const StepOne = ({history}) => (
  <div className="StepOne">
  <h1>Étape 1 : certifier un document numérique</h1>
  <Row>
    <Col xs={12} md={12} className="dropzone-background">
      <FileHashStamper history={history} />
    </Col>
  </Row><br />
  <Row>
    <Col md={12}>
  <Alert bsStyle="warning" className="centered">
    <p className="text-center">Après l'envoi de votre fichier, un document sera <b>automatiquement</b> téléchargé sur votre ordinateur. Il s'agit d'un reçu qui permet de vérifier à l'étape suivante que le document électronique que vous aurez sélectionné est certifié conforme.</p>
  </Alert>
  </Col>
  </Row>
  <Row>
    <Col md={12} className="models-frame">
      <div className="models">
        <Col md={2}>
          <p>Vous n'avez pas de documents à vérifier ? Téléchargez l'un de nos modèles</p>
        </Col>
        <Col md={2}>
          <a href="/datatrust-rib.pdf" download><div className="model1 model">RIB</div></a>
        </Col>
        <Col md={2}>
        <a href="/datatrust-diplome.pdf" download><div className="model2 model">Diplôme</div></a>
        </Col>
        <Col md={2}>
        <a href="/datatrust-contrat.pdf" download><div className="model3 model">Contrat</div></a>
        </Col>
        <Col md={2}>
        <a href="/datatrust-facture.pdf" download><div className="model4 model">Facture</div></a>
        </Col>
        <Col md={2}>
        <a href="/datatrust-dessin-industriel.pdf" download><div className="model5 model">Dessin</div></a>
        </Col>
      </div>
    </Col>
  </Row>
</div>/* End StepOne */
);

export default StepOne;