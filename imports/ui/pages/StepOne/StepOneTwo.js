import React from 'react';
import {Button,
  Row,
  Col,
  Checkbox,
  Radio,
  FormGroup,
  ControlLabel,
  FormControl,
  Form} from 'react-bootstrap';
import FileHashHandler from '../../components/FileHashStamper/FileHashStamper';

const StepOneTwo = () => (
  <div className="StepOneTwo">
  <h1>Étape 1.2 : créer l'empreinte numérique d'un document</h1>
  <Row>
    <Col xs={12} md={12} className="receipt-request-frame">
    <p className="text-center">
        <i className="fa fa-check-square-o fa-4x" aria-hidden="true"></i><br/>
        Empreinte numérique créée avec succès.
      </p>
    </Col>
  </Row><br />

  <Row>
    <Col md={6}>
    <button className="btn btn-success centered">En savoir plus sur l'empreinte numérique</button>
    </Col>
    <Col md={6}>
    <button className="btn btn-success centered">Étape suivante : ancrer l'empreinte numérique</button>
    </Col>
  </Row>

</div>/* End StepOneTwo */
);

export default StepOneTwo;