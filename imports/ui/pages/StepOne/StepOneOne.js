import React from 'react';
import {Button, Col, Row, Alert} from 'react-bootstrap';
import { Link } from 'react-router-dom';

import './StepOneOne.scss';

const StepOne = () => (
  <div className="StepOne">
  <h1>Étape 1 : certifier un document numérique</h1>

  <Row>
    <Col xs={12} md={12} className="receipt-request-frame">
      <p className="text-center">
        <i className="fa fa-check-square-o fa-4x" aria-hidden="true"></i><br/>
        Votre document a bien été enregistré.
      </p>
    </Col>
  </Row><br/>
  <Row>
    <Col md={12}>
  <Alert bsStyle="warning" className="centered">
    <p className="text-center">Le reçu a automatiquement été enregistré dans le dossier "Téléchargement" de votre ordinateur.</p>
  </Alert>
  </Col>
  </Row><br />

  <Row>
    <Col>
      <Link to={"/stepthree"}><button className="btn btn-success centered">Vérifier le document</button></Link>
    </Col>
    {/* <Col md={2}>
      <Link to={"/steptwoone"}><button className="btn btn-success centered">Recevoir le reçu</button></Link>
    </Col> */}
  </Row>

</div>/* End StepOne */
);

export default StepOne;