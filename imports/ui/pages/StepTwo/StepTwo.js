import React from 'react';
import { Col, Row} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './StepTwo.scss';

const StepOne = () => (
  <div className="StepOne">
  <h1>Étape 2 : ancrer l'empreinte numérique</h1>
  <Row>
    <Col xs={12} md={12} className="hash-animation-container">
    <img src="hash.gif" className="hash-animation centered" alt="blockchainpartner-hash" />
    </Col>
  </Row>
  {/* Le bouton suivant soit apparaître lorsque l'animation du dessus disparaît */}
  <Row>
    <Col className="btn-receipt">
      <Link to={"/steptwoone"}><button className="btn btn-success centered">Recevoir le reçu</button></Link>
    </Col>
  </Row>

</div>/* End StepOne */
);

export default StepOne;