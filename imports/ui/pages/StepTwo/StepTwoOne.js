import React from 'react';
import {Button,
  Row,
  Col,
  Checkbox,
  Radio,
  FormGroup,
  ControlLabel,
  FormControl,
  Form} from 'react-bootstrap';
import FileHashHandler from '../../components/FileHashStamper/FileHashStamper';

import './StepTwoOne.scss';

const StepTwoOne = () => (
  <div className="StepTwoOne">
  <h1>Étape 2.1 : ancrer l'empreinte numérique</h1>
  <Row>
    <Col xs={12} md={12} className="receipt-request-frame">
    <h2 className="receipt-title">Envoi reçu d'ancrage</h2>
    <Form type="submit">
        <Row>
          <Col className="formField" md={4} mdOffset={4}>
            <FormGroup controlId="fistname">
              <ControlLabel>Adresse email</ControlLabel>
              <FormControl type="text" placeholder="satoshi.nakamoto@blockchainpartner.fr"/><br />
              <button className="btn btn-success centered">Envoyer</button>
            </FormGroup>
          </Col>
        </Row>
        </Form>
    </Col>
  </Row>

</div>/* End StepTwoOne */
);

export default StepTwoOne;