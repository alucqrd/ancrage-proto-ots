import React from 'react';
import {Button,
  Row,
  Col,
  Checkbox,
  Radio,
  FormGroup,
  ControlLabel,
  FormControl,
  Form} from 'react-bootstrap';
import FileHashHandler from '../../components/FileHashStamper/FileHashStamper';

import './StepTwoTwo.scss';

const StepTwoTwo = () => (
  <div className="StepTwoTwo">
  <h1>Étape 2.2 : ancrer l'empreinte numérique</h1>
  <Row>
    <Col xs={12} md={12} className="receipt-request-frame">
    <img src="email.png" className="email-icon" />
    <h2>Le reçu sera envoyé dès l'ancrage sur la blockchain effectué !</h2>
    <p className="text-center">La réception du reçu prendra quelques minutes. Pour comprendre pourquoi, cliquez sur "En savoir plus sur l'ancrage".</p>
    </Col>
  </Row><br />

  <Row>
    <Col md={6}>
    <button className="btn btn-success centered">En savoir plus sur l'ancrage</button>
    </Col>
    <Col md={6}>
    <button className="btn btn-success centered">Étape suivante : vérifier un document</button>
    </Col>
  </Row>

</div>/* End StepTwoTwo */
);

export default StepTwoTwo;