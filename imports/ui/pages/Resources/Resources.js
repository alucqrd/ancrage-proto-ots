import React from 'react';
import { Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Ressources.scss';

const Resources = () => (
  <div className="Resources">
    <h1>Ressources</h1>
    <Row className="all-ressources">
      <Col md={4} className="article1 articles">
        <h2>L’empreinte numérique</h2><br/>
        <p>L’empreinte numérique est l’équivalent de l’empreinte digitale mais pour les
          données : chaque donnée a sa propre empreinte numérique. Si une donnée texte
          telle qu’un document PDF est modifiée, ne serait-ce...<br/><br/>
          <Link to={"/resources/fingerprint"}>Lire la suite</Link>
        </p>
      </Col>
      <Col md={4} className="article2 articles">
        <h2>L’ancrage Blockchain</h2><br/>
        <p>L’ancrage blockchain consiste à ajouter l’empreinte numérique d’une donnée à
          une transaction sur une blockchain publique (Bitcoin ou Ethereum). Sur
          datatrust, nous utilisons la blockchain Ethereum pour des raisons...<br/><br/>
          <Link to={"/resources/ancrage"}>Lire la suite</Link>
        </p>
      </Col>
      {/* <Col md={4} className="article3 articles">
        <h2>Preuve d'authenticité</h2><br/>
        <p>La blockchain est la technologie qui se cache derrière la désormais fameuse
          cryptomonnaie Bitcoin. Cette technologie a été consacrée par un article publié
          en 2015 par The Economist et intitulé “The Trust...<br/><br/>
          <Link to={"/resources/authenticite"}>Lire la suite</a>
        </p>
      </Col> */}
    </Row>
  </div>
);

export default Resources;
