import React from 'react';
import {Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Ancrage = () => (
  <div className="Ancrage">
    <h1>L’ancrage Blockchain</h1>
    <Row>
      <Col md={12}>
        <img
          src="/article-ancrage-blockchain.jpg"
          className="article-image centered"
          alt="datatrust-article-ancrage-blockchain"/>
      </Col>
    </Row>
    <Row className="all-ressources">
      <Col md={12} className="articles-content">
        <p>
          <b>Techniquement, comment fonctionne l’ancrage ?</b>
        </p>
        <p>L’ancrage blockchain consiste à <b>enregistrer <Link to={"/resources/fingerprint"}>l’empreinte numérique</Link> d’une
        donnée dans une transaction sur une blockchain publique</b> (par exemple, Bitcoin ou Ethereum). 
        Sur datatrust, nous utilisons le serveur Open Time Stamp qui ancre les données sur la blockchain Bitcoin.</p><br/>

        <p>
          <b>Ancrage et Arbre de Merkle</b>
        </p>

        <p>L’arbre de Merkle permet de répondre à un enjeu de scalabilité : comment
          ancrer plusieurs milliers de données dans une même transaction blockchain. Il lui permet de regrouper toutes ses données et
          d’ancrer l’empreinte numérique qui fait référence à la racine de l’arbre.
        </p><br/>

        <p>
          <b>Pourquoi l’ancrage n’est pas automatique ?</b>
        </p>

        <p>
          <b>I/ La fréquence d’ancrage</b><br/>
          La fréquence d’ancrage aura une incidence sur le coût global de la solution
          d’ancrage : plus l’ancrage est régulier plus les frais liés seront élevés car il
          faudra payer les frais de transaction plus souvent.</p>

        <p>
          <b>II/ Le temps de validation des blocs</b><br/>
          Si on décidait d’ancrer automatiquement toutes les données, ce qui n’est
          vraiment pas intéressant d’un point de vue coût, il faudrait quand même attendre
          le temps que soit validé le bloc dans lequel figure l’empreinte numérique de la
          donnée. Si on utilise la blockchain Bitcoin dont les blocs sont validés toutes
          les 10min, il faudrait à minima attendre ce temps-là.
        </p><br/>

        <p>Voir aussi : <Link to={"/resources/authenticite"}>Créer une preuve d'authenticité sur Blockchain</Link>
        </p>
      </Col>
    </Row>
  </div>
);

export default Ancrage;