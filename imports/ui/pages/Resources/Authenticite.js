import React from 'react';
import {Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Authenticite = () => (
  <div className="Authenticite">
    <h1>Créer une preuve d'authenticité sur Blockchain</h1>
    <Row>
      <Col md={12}>
        <img
          src="/article-ancrage-blockchain.jpg"
          className="article-image centered"
          alt="datatrust-article-ancrage-blockchain"/>
      </Col>
    </Row>
    <Row className="all-ressources">
      <Col md={12} className="articles-content">
        <p>La blockchain est la technologie qui se cache derrière la désormais fameuse
          cryptomonnaie Bitcoin. Cette technologie a été consacrée par un article publié
          en 2015 par The Economist et intitulé “The Trust Machine” (La machine à
          confiance).</p><br/>

        <p>
          <b>1/ La confiance sans intermédiaire</b>
        </p>
        <p>Jusqu’à aujourd’hui, un réseau décentralisé ne pouvait être sécurisé : une
          donnée pouvait être facilement copiée ou falsifiée. A l’inverse, les réseaux
          sécurisés, comme celui de votre banque, devait être centralisés. La Blockchain
          allie décentralisation et authentification des données.</p>

        <p>Définie comme un protocole sécurisé d’échange d’information de pairs à pairs
          sur un réseau inviolable et distribuée, la blockchain est un registre sur lequel
          sont inscrites chronologiquement les transactions de manière immuable.
          L’inscription d’un échange sur le registre n’est possible qu’après une
          validation des mineurs par consensus. En pratique, une modification du registre
          n’est possible qu’en contrôlant plus de 51% des mineurs : le réseau est donc
          inviolable.</p><br/>

        <p>
          <b>2/ Une technologie de chiffrement probante</b>
        </p>
        <p>Le fonctionnement de la Blockchain est aussi simple qu’il mobilise des
          technologies complexes. Les outils cryptographiques sur lequel elle repose sont
          les mêmes que ceux de la signature électronique (Notamment la fonction de hash
          SHA-2 et l’algorithme de chiffrement asymétrique RSA). Or la preuve électronique
          s’appuie précisément sur cette technologie.</p>

        <p>Puisque la signature, la preuve et la Blockchain utilisent le même algorithme
          de chiffrement et les mêmes outils, pourquoi ne pas utiliser celle-ci dernière
          comme « une machine à prouver » ?</p>

        <p>Les avantages sont de plusieurs ordres :
          <ul>
            <li>Le code de la Blockchain est totalement open source, c’est à dire librement
              accessible et sûr;</li>
            <li>L’inscription d’une information sur la Blockchain ne coûte que quelques
              centimes (En février 2017, l’inscription d’un bit coûte 0,0000001300 Bitcoins,
              soit 0,0012 $);</li>
            <li>La Blockchain est disponible partout et en permanence ;</li>
            <li>La Blockchain est sécurisée vis à vis des hackeurs mais aussi des autorités
              publiques</li>
          </ul>
        </p><br/>

        <p>
          <b>3/ Le fonctionnement de la preuve électronique</b>
        </p>
        <p>La preuve sert à établir, pour la personne qui s’en prévaut, la réalité d’un
          fait allégué. L’article 1366 du code civil dispose que « l’écrit électronique a
          la même force probante que l’écrit sur support papier, sous réserve que puisse
          être dûment identifiée la personne dont il émane et qu’il soit établi et
          conservé dans des conditions de nature à en garantir l’intégrité ».</p>

        <p>L’intégrité d’un acte numérique peut être garantie par une empreinte
          électronique permettant de vérifier facilement son intégrité. Contrairement au
          papier, le numérique se caractérise par sa réplicabilité. L’empreinte permet de
          s’assurer facilement que pas un bit du document n’a été modifié (L’empreinte de
          ce document en SHA-1 est : 821C2D2523B6EBE90F8A7BA001FF7289682019B1. Le
          changement d’un signe génère une empreinte totalement différente :
          2A37E45D00F0771A415CD53EA8B5B395AF57F319 ( !)).</p><br/>

        <p>
          <b>4/ Le lien entre le document et la signature : le chiffrement</b>
        </p>
        <p>La signature, quant à elle, est indispensable à la perfection de l’acte en
          identifiant son auteur. L’article 1367 du code civil définit la signature
          électronique en « un procédé fiable d’identification garantissant son lien avec
          l’acte ».</p>

        <p>Techniquement, la signature électronique consiste à envoyer un document et
          son empreinte chiffrée au destinataire. La comparaison de l’empreinte et du
          document assure l’intégrité de ce dernier quand le chiffrement identifie le
          signataire. Mais comment s’assurer de l’identité réelle du signataire ?</p><br/>

        <p>
          <b>5/ Le lien entre la signature et l’identité : la certification</b>
        </p>
        <p>La dématérialisation du processus est limitée par le lien entre les clefs de
          chiffrement et l’identité des personnes. Pour cela, l’Ordonnance du 8 novembre
          2005 a prévu des organismes de certification agréés par l’ANSSI qui vérifient
          l’identité des personnes avant de leur délivrer un certificat, ces derniers
          permettant au destinataire de vérifier l’identité du signataire.</p><br/>

        <p>
          <b>6/ Le problème de la certification pour la Blockchain</b>
        </p>
        <p>La Blockchain ne permet pas d’apporter de lien juridique entre l’identité du
          signataire et sa signature : n’importe qui peut, par exemple, créer une adresse
          Bitcoin (Une adresse Bitcoin correspond à une clef de chiffrement publique :
          ​18s32jT9Ws7JkX6RkqLgmn3b8SnnbnHw1j ) sans révéler son identité.</p>

        <p>Par contre, elle pourrait servir de registre de preuve d’antériorité de
          document. En effet, l’empreinte déposée sur la Blockchain permet :
          <ul>
            <li>De vérifier l’intégrité du document ;</li>
            <li>D’horodater le document ;</li>
            <li>De créer un lien entre le signataire et le document.</li>
          </ul>
        </p>

        <p>Juridiquement, la Blockchain a été définie par l’Ordonnance du 28 avril 2016
          relative aux bons de caisse comme « un dispositif d’enregistrement [de
          l’émission des bons de caisse] électronique partagé permettant
          l’authentification de ces opérations ».</p>

        <p>Cette définition reprend les idées d’enregistrement des échanges, de registre
          partagé et d’authentification. Le groupe de travail chargé de mettre en œuvre
          cette Blockchain s’est réuni mais, à ce jour, aucun décret d’application n’a été
          adopté.</p><br/>

        <p>
          <b>7/ Les limites de la Blockchain en matière de preuve</b>
        </p>
        <p>En revanche, la preuve d’antériorité sur la Blockchain connaît trois obstacles :

          <ol>
            <li>Le premier concerne la qualification juridique du réseau. La Blockchain
              n’est, pour l’instant, qu’un « dispositif d’enregistrement partagé ». Quelle
              valeur un juge accorde-t-il à l’authentification par un réseau distribué ? Et
              surtout, quelle valeur accorde-t-il à une preuve fournie par une technologie non
              agréée par l’ANSSI ? Une réponse sera peut-être apportée par les décrets
              d’application relatifs aux mini-bons en fonction de la Blockchain retenue
              (Bitcoin par exemple).</li>

            <li>Ensuite, l’inscription sur une Blockchain nécessite de posséder de la
              crypto-monnaie. En effet, la Blockchain n’enregistre que des transactions
              payantes.</li>

            <li>Enfin, la Blockchain ne pardonne aucune perte ni oubli de clef. Dans ce cas,
              le lien entre le signataire et la preuve sont impossibles à rapporter.</li>
          </ol>
        </p><br/>

        <p>Voir aussi : <Link to={"/resources/fingerprint"}>L'empreinte numérique</Link>
        </p>
      </Col>
    </Row>
  </div>
);

export default Authenticite;