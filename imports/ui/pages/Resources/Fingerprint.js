import React from 'react';
import {Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Fingerprint = () => (
  <div className="Fingerprint">
    <h1>L'empreinte numérique</h1>
    <Row>
      <Col md={12}>
    <img src="/article-empreinte-numerique.jpg" className="article-image centered" alt="datatrust-article-empreinte-numerique" />
      </Col>
    </Row>
    <Row className="all-ressources">
      <Col md={12} className="articles-content">
        <p>
          <b>Qu’est-ce qu’une empreinte numérique ?</b>
        </p>
        <p>L’empreinte numérique est l’équivalent de l’empreinte digitale mais pour les
          données : chaque donnée a sa propre empreinte numérique. Si une donnée texte sur un document PDF est modifiée, ne serait-ce que d’une virgule, son
          empreinte numérique sera totalement différente.</p>

        <p>D’un point de vue technique, une empreinte numérique est une suite de
          caractères (chiffres et lettres) unique. Cette suite de caractères est appelée
          hash.</p><br />

        <p>
          <b>Pourquoi créer une empreinte numérique et ne pas ajouter directement une
            donnée sur la blockchain ?</b>
        </p>

        <p>La taille des blocs étant limitée, seules des données brutes (chiffres et
          textes) peuvent être ajoutées sur une blockchain.</p>

        <p>Les données au format PDF, JPEG, PPT, etc. ne sont donc pas ajoutées
          directement. C’est l’empreinte numérique d’une donnée qui est enregistrée sur la
          blockchain, et non la donnée elle-même. L’empreinte numérique permet de créer un
          lien entre une donnée et la blockchain.</p><br />

        <p>
          <b>Comment une empreinte numérique est-elle créée ?</b>
        </p>

        <p>L’empreinte numérique (hash) est créée à partir d’une fonction de hash
          cryptographique appelée SHA256. Cette fonction est très largement utilisée par
          les personnes qui souhaitent “hasher” une donnée, c’est à dire créer l’empreinte
          numérique de cette dernière. A l’heure actuelle, il s’agit de la fonction de
          hash la plus robuste.</p><br />

          <p>Voir aussi : <Link to={"/resources/ancrage"}>L'ancrage blockchain</Link></p>
      </Col>
    </Row>
  </div>
  );
  
  export default Fingerprint;