import React from 'react';
import Page from '../Page/Page';

const Privacy = () => (
  <div className="Privacy">
    <Page
      title="Conditions générales d’utilisation du site datatrust.co"
      subtitle="Dernière mise à jour le 28/11/2017"
      page="privacy"
    />
  </div>
);

export default Privacy;
