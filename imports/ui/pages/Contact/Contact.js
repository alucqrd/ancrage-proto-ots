import React from 'react';
import ContactForm from '../../components/ContactForm/ContactForm'
import './Contact.scss';

const Contact = () => (
  <div className="Contact">
  <p>Afin que nous puissions vous recontacter, veuillez remplir le formulaire suivant.</p><br />
    <ContactForm />
  </div>
);

export default Contact;
