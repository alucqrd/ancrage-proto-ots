import React from 'react';
import {Button, Col, Row} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Pricing.scss';

const Pricing = () => (
  <div className="Pricing">
  <h1 className="nos-offres">Nos offres</h1>
  <Row>
    <Col xs={12} md={5} mdOffset={1} className="ancrage-en-ligne frames">
      <h2>Marque blanche</h2>
      <p>Vous souhaitez utiliser et commercialiser le service d'ancrage DataTrust pour répondre à vos besoins métiers, contactez-nous pour établir un devis.</p>
      <Col>
      <a href="mailto:chloe@blockchainpartner.fr?subject=Demande de devis utilisation datatrust en marque blanche&body=Bonjour,
            %0D%0A%0D%0AJe suis [indiquez votre nom ici], pouvez-vous me recontacter pour établir un devis pour l'utilisation du datatrust en marque blanche ?%0D%0A%0D%0AVoici mes coordonnées téléphoniques : [indiquez ici votre numéro de téléphone pour être recontacté]%0D%0A%0D%0ACordialement,"><button className="btn btn-success buy-ancres centered">Demander un devis</button></a>
      </Col>
    </Col>
    <Col xs={12} md={5} className="interfacage frames">
      <h2>Interfaçage sur-mesure</h2>
      <p>Nous évaluons votre besoins afin de développer les API nécessaires à l'interfaçage entre vos systèmes d'information et la solution datatrust.</p>
      <Row>
        <Col>
          <Link to={"/contact"}><button className="btn btn-success being-contacted centered">Être contacté par un expert</button></Link>
        </Col>
      </Row>
    </Col>
  </Row>
</div>/* End pricing */
);

export default Pricing;