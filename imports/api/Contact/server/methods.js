import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

Meteor.methods({
    'utility.submitContactForm': function submitContactForm(firstName, lastName, email, phone, company) {
      check(firstName, String);
      check(lastName, String);
      check(email, String);
      check(phone, String);
      check(company, String);

      let text = firstName + ' ' + lastName + ' de l\'entreprise ' + company + ' souhaite être recontacté à propos de l\'ancrage DataTrust.\nEmail : ' + email + '\n' + 'Telephone : ' + phone;

      // Let other method calls from the same client start running, without
      // waiting for the email sending to complete.
      this.unblock();
      Email.send({from: 'ancrageblockchainpartner@gmail.com', to: 'chloe@blockchainpartner.fr', subject: 'Datatrust Contact', text: text});
    }
  });
  