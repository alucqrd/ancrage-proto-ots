import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import OpenTimestamps from 'javascript-opentimestamps';
import moment from 'moment';
import Stamps from '../../Stamps/Stamps';

Meteor.methods({
  'opentimestamps.stampHash': async function StampHash(hashStr) {
    check(hashStr, String);

    console.log("STAMPING HASH : " + hashStr);
    let hashByteArray = OpenTimestamps.Utils.hexToBytes(hashStr);
    let detached = OpenTimestamps.DetachedTimestampFile.fromHash(new OpenTimestamps.Ops.OpSHA256(), hashByteArray);
    try {
      await OpenTimestamps.stamp(detached);
    }
    catch (error) {
      console.log(error);
    }

    let serializedDetached = detached.serializeToBytes();

    Stamps.insert({
      fileHash: hashStr,
      recipeFile: serializedDetached,
    });

    return serializedDetached;
  },
  'opentimestamps.verify': async function StampHash(fileHashStr, otsFile) {
    check(fileHashStr, String);
    check(otsFile, Uint8Array);

    console.log("VERIFYING HASH : " + fileHashStr);


    // Search for entry in our db
    let mongoEntry = await Stamps.findOne({fileHash: fileHashStr, recipeFile: otsFile});

    // Verify via OTS
    let hashByteArray = OpenTimestamps.Utils.hexToBytes(fileHashStr);
    let detached = OpenTimestamps.DetachedTimestampFile.fromHash(new OpenTimestamps.Ops.OpSHA256(), hashByteArray);
    let otsDetached = OpenTimestamps.DetachedTimestampFile.deserialize(otsFile);

    let verifyRes;
    let fileMatched = true;
    try {
      await OpenTimestamps.verify(otsDetached, detached).then(verifyResult => {
        // return a timestamp if verified, undefined otherwise.
        verifyRes = verifyResult;
      });
    }
    catch (error) {
      console.log(error);
      if(error == "File does not match original!")
        fileMatched = false;
    }

    if(!fileMatched)
      return "Le fichier .ots de reçu ne correspond pas au fichier donné en entrée.";

    else if(verifyRes)
      return "Le fichier a bien été ancré dans la Blockchain le " + moment.unix(verifyRes).format('Do MMMM YYYY, à h:mm:ss a') + ".";

    else if(!verifyRes && mongoEntry)
      return "Le fichier a été soumis pour être ancré le " + moment(mongoEntry.createdAt).format('Do MMMM YYYY, à h:mm:ss a') + ".";

    return "Pas d'ancrage trouvé dans la blockchain.";
  }
});