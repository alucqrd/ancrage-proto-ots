import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Stamps from './Stamps';

Meteor.methods({
  'stamps.find' : function stampsAdd(fileHashStr, recipeFile) {
    check(fileHash, String);
    check(recipeFile, Uint8Array);

    try {
      return Stamps.insert({ fileHash: fileHash, recipeFile: recipeFile });
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  }
});