/* eslint-disable consistent-return */

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Stamps = new Mongo.Collection('Stamps');

Stamps.allow({
  insert: () => false,
  update: () => false,
  remove: () => false
});

Stamps.deny({
  insert: () => true,
  update: () => true,
  remove: () => true
});

Stamps.schema = new SimpleSchema({
  createdAt: {
    type: String,
    label: 'The date the file was stamped on the website.',
    autoValue() {
      if (this.isInsert) return (new Date()).toISOString();
    }
  },
  fileHash: {
    type: String,
  },
  recipeFile: {
    type: Uint8Array,
  },
});

Stamps.attachSchema(Stamps.schema);

export default Stamps;