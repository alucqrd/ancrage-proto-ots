### Article 1 - Objet
Datatrust.co est un service en ligne d’inscription de preuve numérique dans la blockchain. Ces conditions générales d’utilisation (“CGU”) ont pour objet d'encadrer les droits et les obligations du service, encore à l’état de démo, et de ses utilisateurs. 
En utilisant le service, l’utilisateur est présumé avoir accepté sans restriction les CGU. 

### Article 2 - Définition
“Utilisateur” : toute personne physique ou morale qui utilise le site conformément aux CGU
“Service” : désigne la Certification de la donnée de l’Utilisateur ainsi que la vérification ultérieure de cette Certification.
“Société” : désigne Blockchain France Associés qui édite le site et assure le fonctionnement du service conformément aux CGU. 
“Certification de données” : désigne la création de la Preuve Numérique d’une donnée de l’Utilisateur sous la forme d’un hash et son inscription dans la blockchain pour en assurer l’horodatage, l’intégrité et l’accessibilité. 
“Preuve numérique” : désigne la création d’un hash cryptographique d’une donnée depuis le navigateur de l’Utilisateur.

### Article 3 - Service

#### 3.1 Description des services

Datatrust.co propose un service d’inscription en ligne de preuve numérique dans la blockchain. Le Service se compose : 
* d’une démo de certification d’une donnée;
* d’une démo de vérification de la certification préalable d’une donnée;
* de divers contenus accessibles sur le site (ressources, contact,etc.)

L’accès au Service, encore à l’état de démo, est libre et gratuit. La donnée que l’Utilisateur souhaite certifier doit, nécessairement, être un fichier numérique (.jpeg, .pdf, .doc, .txt etc.). 

#### 3.2 Continuité du service
La Société met tout en oeuvre pour assurer la continuité du Service. Cependant, il pourra être interrompu à tout moment en cas de maintenant, de défaillance ou par choix de la Société.

### Article 4 - Propriété Intellectuelle

#### 4.1 - Sur les éléments distinctifs du site
Tous les signes distinctifs, marque, logo, charte graphique, architecture et contenus du site sont la propriété exclusives de celui-ci. Leurs utilisation ou reproduction sans autorisation préalable de la Société sont interdites conformément à la Loi.

#### 4.2 - Sur les contenus de l'utilisateur
L’Utilisateur garantit la Société de toutes violations des droits de propriété intellectuelle des données qu’il souhaite certifier au moyen du Service.

### Article 5 - Responsabilité

#### 5.1 - Sur les contenus du site
La Société ne pourra voir sa responsabilité engagée par l’Utilisateur en cas d’absence de fiabilité des informations publiées sur le site. Elles sont fournies à titre gratuit et informatif.

#### 5.2 -  Sur la fiabilité du service
Si la Société s’efforce d’assurer la fiabilité du service et la confidentialité des données, tant au stade de la création de la preuve numérique (hash) qu’au stade de l’inscription de celle-ci dans la blockchain, sa responsabilité ne pourra pas être engagé en cas d’utilisation gratuite du service.

#### 5.3 - Sur la fiabilité de la blockchain
La blockchain est un protocole informatique indépendant et autonome sur lequel ni la Société, ni aucun acteur identifié n’exercent d’autorité. Une défaillance de ce protocole, quelqu’en soit la raison, sera assimilée à un cas de force majeur et exonérera la Société de la responsabilité sur les dommages causés.

### Article 6 - Liens hypertexte
Le site datatrust.co peut contenir des liens hypertexte donnant accès à d’autres sites web édités et gérés par des tiers. La Société ne pourra être tenue responsable directement ou indirectement dans le cas où ces sites tiers ne respectent pas les dispositions légales.

### Article 7 - Durée
Les CGU sont applicables pour une durée indéterminée. Elles peuvent être résiliées ou modifiées de manière unilatérale par la Société à n’importe quel moment.

### Article 8 - Droit applicable et tribunal compétent
Les CGU sont régis par le droit français et leur interprétation en cas de litige judiciaire relèvera de la compétence des tribunaux francais.