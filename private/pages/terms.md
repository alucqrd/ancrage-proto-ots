Le site www.datatrust.co est édité par la société Blockchain France Associés, SAS dont le siège social est situé 43 avenue de la République, 75011 à Paris, et enregistrée au RCS de Paris sous le numéro de SIRET 818 906 646 00026.

TVA intracommunautaire n° FR77818906646.

Directeur de la publication du site : Claire Balva

Contact : [contact@blockchainpartner.fr](mailto:contact@blockchainpartner.fr)