import { Meteor } from 'meteor/meteor';
import moment from 'moment';
import 'moment/locale/fr';

// Set moment in french
Meteor.startup(() => {
  moment.locale('fr');
});