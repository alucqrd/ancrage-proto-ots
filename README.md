Ancrage
===============================================

---------

I. Getting started
------------------
Installer meteor (https://www.meteor.com/) et NodeJS (https://nodejs.org/en/)

Cloner le repo :
```git clone https://github.com/Blockchainpartner/ancrage.git```

Aller à la racine du projet

Installer les dependencies :
```meteor npm install```

Pour démarrer le serveur :
```npm start```

Accéder au dashboard : http://localhost:3000

II. DEPLOYMENT
------------------

Installer meteor up :
```npm install -g mup```

Aller dans dossier .deploy à la racine, vérifier dans le fichier .deploy/mup.js que les settings correspondent bien au serveur Amazon.

Pour déployer (direct dans un docker) :
```mup deploy```

Coffee is done and ready, pour plus d'infos, voir http://meteor-up.com/getting-started.html